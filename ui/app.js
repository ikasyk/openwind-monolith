import React from 'react';
import { render } from 'react-dom';

render(
    <div>REACT test</div>,
    document.getElementById('content')
);

