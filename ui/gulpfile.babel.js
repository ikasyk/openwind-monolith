import gulp from 'gulp';
import path from 'path';
import rimraf from 'rimraf';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-sass';
import concat from 'gulp-concat';
import cleanCSS from 'gulp-clean-css';
import webpack from 'webpack';

const paths = {
    srcPublic: 'src/public/**',
    srcScss: 'src/**/*.scss',
    dist: 'dist'
};

function clean(done){
    rimraf(paths.dist, done);
}

function bundle(done) {
    webpack({
        plugins: [
            new webpack.optimize.OccurenceOrderPlugin(),
        ],
        entry: ['babel-polyfill', './app.js'],
        module: {
            loaders: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules)/,
                    loader: 'babel',
                    query: {
                        presets: ['es2015', 'react', 'stage-0']
                    }
                }
            ]
        },
        resolve: {
            extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json'],
        },
        output: {
            path: path.join(__dirname, `./${paths.dist}/js`),
            filename: 'app.js'
        }
    }, (err, stats) => {
        if (err) throw new gutil.PluginError('webpack:build', err);
        done();
    });
}

function styles(done) {
    gulp.src(paths.srcScss)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(concat('main.css'))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(`${paths.dist}/css`));
    done();
}

function publicFiles(done) {
    gulp.src(paths.srcPublic)
        .pipe(gulp.dest(paths.dist));
    done();
}

exports.build = gulp.series(clean, bundle, styles, publicFiles);