package org.openwind.util;

import org.apache.commons.lang3.StringUtils;

public class AuthenticationUtils {
    public static boolean isEmail(String str) {
        return StringUtils.isNotBlank(str) && StringUtils.contains(str, '@');
    }
}
