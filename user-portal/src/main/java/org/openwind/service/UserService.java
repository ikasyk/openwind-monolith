package org.openwind.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import org.apache.commons.lang3.time.DateUtils;
import org.openwind.core.security.SecurityConstants;
import org.openwind.core.security.SecurityHelper;
import org.openwind.exception.InvalidCredentialsException;
import org.openwind.model.AuthenticationRequest;
import org.openwind.model.entity.User;
import org.openwind.repository.UserRepository;
import org.openwind.util.AuthenticationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.jwt.Jwt;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ApplicationContext applicationContext;

    public User findById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public User findByAuthentication(AuthenticationRequest authenticationRequest) {
        String principal = authenticationRequest.getLogin();
        User expectedUser = userRepository.findByEmailOrLogin(principal);

        if (expectedUser == null) {
            throw new InvalidCredentialsException("User with this login or e-mail not found");
        }

        if (!passwordEncoder.matches(authenticationRequest.getPassword(), expectedUser.getPassword())) {
            throw new InvalidCredentialsException("Invalid password");
        }

        return expectedUser;
    }

    public String getAuthenticationHeaderForUser(User user) {
        return SecurityConstants.AUTH_TOKEN_PREFIX + getAuthenticationTokenForUser(user);
    }

    private String getAuthenticationTokenForUser(User user) {
        return Jwts.builder()
                .setClaims(getUserTokenClaims(user))
                .setExpiration(getUserTokenExpirationTime())
                .signWith(SecurityHelper.getSignatureKey(applicationContext))
                .compact();
    }

    private Date getUserTokenExpirationTime() {
        return DateUtils.addSeconds(new Date(), SecurityConstants.AUTH_TOKEN_LIFETIME);
    }

    private Claims getUserTokenClaims(User user) {
        Claims claims = new DefaultClaims();
        claims.put(SecurityHelper.USER_ID_CLAIM, user.getId());
        claims.put(SecurityHelper.USER_NAME_CLAIM, user.getLogin());
        return claims;
    }
}
