package org.openwind.service;

import org.openwind.model.entity.User;
import org.openwind.repository.UserRepository;
import org.openwind.model.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class UserRegistrationService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public Long createUserFromRequest(CreateUserRequest request) {
        User user = mapRequestToUser(request);
        user.setDefaultLang(LocaleContextHolder.getLocale());
        user.setRegistrationDate(LocalDateTime.now());
        userRepository.save(user);
        return user.getId();
    }

    private User mapRequestToUser(CreateUserRequest request) {
        User user = new User();
        user.setLogin(request.getLogin());
        user.setEmail(request.getEmail());
        user.setGender(request.getGender());
        user.setBirthDate(request.getBirthDate());

        user.setPassword(
                passwordEncoder.encode(request.getPassword())
        );
        return user;
    }
}
