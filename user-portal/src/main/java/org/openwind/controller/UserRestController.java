package org.openwind.controller;

import lombok.extern.slf4j.Slf4j;
import org.openwind.core.security.SecurityConstants;
import org.openwind.model.AuthenticationRequest;
import org.openwind.model.AuthenticationResponse;
import org.openwind.model.CreateUserRequest;
import org.openwind.model.entity.User;
import org.openwind.service.UserRegistrationService;
import org.openwind.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/users")
@Slf4j
public class UserRestController {
    @Autowired
    private UserRegistrationService userRegistrationService;

    @Autowired
    private UserService userService;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Long create(@RequestBody @Valid CreateUserRequest createUserRequest,
                       HttpServletRequest httpServletRequest) {
        if (createUserRequest.getTimeZone() == null) {
            createUserRequest.setTimeZone(RequestContextUtils.getTimeZone(httpServletRequest));
        }
        Long id = userRegistrationService.createUserFromRequest(createUserRequest);
        log.debug("User ID = {} created", id);
        return id;
    }

    @PermitAll
    @PostMapping(
            path = "/authorize",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void authorize(@RequestBody AuthenticationRequest authenticationRequest,
                                            HttpServletResponse servletResponse) {
        User user = userService.findByAuthentication(authenticationRequest);

        servletResponse.setHeader(SecurityConstants.AUTH_HEADER, userService.getAuthenticationHeaderForUser(user));
    }

    @GetMapping(path = "/")
    public String someTestResource() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "Accessed! User " + authentication.getPrincipal();
    }
}
