package org.openwind.repository;

import org.openwind.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByLogin(String login);

    User findByLogin(String login);

    @Query("select u from org.openwind.model.entity.User u where u.login = ?1 or u.email = ?1")
    User findByEmailOrLogin(String principal);
}
