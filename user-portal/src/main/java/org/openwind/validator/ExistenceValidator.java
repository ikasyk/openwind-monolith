package org.openwind.validator;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.openwind.constraints.ExistenceCheck;
import org.openwind.core.util.ReflectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Id;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Slf4j
public class ExistenceValidator implements ConstraintValidator<ExistenceCheck, String>, ApplicationContextAware {
    private String dataField;
    private ExampleMatcher fieldMatcher;
    private JpaRepository<?, ?> repositoryBean;
    private Class<?> entityClass;

    private ApplicationContext context;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        Object probe;
        try {
            probe = ReflectionUtils.createProbe(entityClass, dataField, value);
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            log.error("Probe creation for type {}, field {} and value {} is failed", entityClass.getName(), dataField, value, e);
            return false;
        }

        return !repositoryBean.exists((Example) Example.of(probe, fieldMatcher));
    }

    @Override
    public void initialize(ExistenceCheck constraintAnnotation) {
        dataField = constraintAnnotation.dataField();

        var repository = constraintAnnotation.repository();
        repositoryBean = context.getBean(repository);

        entityClass = constraintAnnotation.entityType();
        List<Field> idFields = FieldUtils.getFieldsListWithAnnotation(
                entityClass, Id.class
        );

        fieldMatcher = ExampleMatcher.matching()
                .withIgnorePaths(idFields.stream().map(Field::getName).toArray(String[]::new))
                .withMatcher(dataField, ExampleMatcher.GenericPropertyMatchers.ignoreCase());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
