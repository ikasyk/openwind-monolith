package org.openwind.exception;

public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException(String login) {
        super("Username " + login + " is used.");
    }
}
