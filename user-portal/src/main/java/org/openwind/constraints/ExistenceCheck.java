package org.openwind.constraints;

import org.openwind.validator.ExistenceValidator;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = ExistenceValidator.class)
@Documented
public @interface ExistenceCheck {
    String message() default "{org.openwind.validator.objectAlreadyExists.message}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    Class<? extends JpaRepository<?, ?>> repository();

    String dataField() default "";

    Class<?> entityType();
}
