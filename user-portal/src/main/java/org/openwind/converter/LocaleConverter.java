package org.openwind.converter;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import java.util.Locale;

public class LocaleConverter implements AttributeConverter<Locale, String> {
    @Override
    public String convertToDatabaseColumn(Locale attribute) {
        if (attribute != null) {
            return attribute.toLanguageTag();
        }
        return null;
    }

    @Override
    public Locale convertToEntityAttribute(String dbData) {
        if (!StringUtils.isEmpty(dbData)) {
            return Locale.forLanguageTag(dbData);
        }
        return null;
    }
}
