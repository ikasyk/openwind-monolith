package org.openwind.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AuthenticationRequest {
    @NotBlank
    private String login;

    @NotNull
    private String password;
}
