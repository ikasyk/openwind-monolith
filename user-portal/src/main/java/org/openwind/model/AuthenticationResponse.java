package org.openwind.model;

import lombok.Data;

@Data
public class AuthenticationResponse {
    private String token;
}
