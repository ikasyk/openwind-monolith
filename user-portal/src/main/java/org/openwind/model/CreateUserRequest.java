package org.openwind.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.openwind.constraints.ExistenceCheck;
import org.openwind.model.entity.Gender;
import org.openwind.model.entity.User;
import org.openwind.repository.UserRepository;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.TimeZone;

@Data
@NoArgsConstructor
@ToString
public class CreateUserRequest {
    @NotBlank
    @Length(min = 3, max = 32)
    @Pattern(regexp = "^[a-z_][a-z0-9_]+$", flags = {Pattern.Flag.CASE_INSENSITIVE})
    @ExistenceCheck(repository = UserRepository.class, entityType = User.class, dataField = "login")
    private String login;

    @NotEmpty
    @Length(min = 8)
    private String password;

    @NotNull
    private Gender gender;

    @NotNull
    @Email
    @ExistenceCheck(repository = UserRepository.class, entityType = User.class, dataField = "email")
    private String email;

    @NotNull
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate birthDate;

    private TimeZone timeZone;
}
