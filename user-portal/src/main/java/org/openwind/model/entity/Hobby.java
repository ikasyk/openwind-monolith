package org.openwind.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "HOBBY")
@Data
public class Hobby {
    @Column(name = "HOBBY_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_HOBBY")
    @SequenceGenerator(name = "S_HOBBY", sequenceName = "S_HOBBY")
    @Id
    private Long id;

    @Column(name = "HOBBY_NAME")
    private String name;
}
