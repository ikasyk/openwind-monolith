package org.openwind.model.entity;

import java.util.Arrays;

public enum Gender {
    FEMALE(0),
    MALE(1);

    private Integer id;

    Gender(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static Gender fromId(Integer id) {
        return Arrays.stream(values())
                .filter((x) -> x.getId().equals(id))
                .findAny()
                .orElseThrow();
    }
}
