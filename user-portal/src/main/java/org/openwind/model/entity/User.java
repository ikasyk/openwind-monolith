package org.openwind.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.openwind.converter.GenderConverter;
import org.openwind.converter.LocaleConverter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;

@Entity(name = "USERS")
@Data
@NoArgsConstructor
public class User {
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_USERS")
    @SequenceGenerator(name = "S_USERS", sequenceName = "S_USERS", allocationSize = 1, initialValue = 10)
    @Id
    private Long id;

    @Column(name = "USER_LOGIN")
    @NonNull
    private String login;

    @Column(name = "USER_EMAIL")
    private String email;

    @Column(name = "USER_PASSWORD")
    @NotNull
    private String password;

    @Column(name = "USER_GENDER")
    @Convert(converter = GenderConverter.class)
    @NotNull
    private Gender gender;

    @Column(name = "USER_BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name = "USER_REGISTRATION_DATE")
    @NonNull
    private LocalDateTime registrationDate;

    @Column(name = "USER_LANG")
    @Convert(converter = LocaleConverter.class)
    @NotNull
    private Locale defaultLang;

    @Column(name = "USER_TIMEZONE")
    private String timezone;
}
