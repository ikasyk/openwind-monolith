package org.openwind.core.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = TestApplication.class)
@Sql({"/core-db/schema.sql", "/core-db/data.sql"})
public class SecurityAuthorizationTest {
    @Value("${security.jwt.token-secret}")
    private String jwtSignatureKey;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())

                .build();
    }

    @Test
    public void unauthorizedWithoutHeader() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void unauthorizedEmptyHeader() throws Exception {
        mockMvc.perform(get("/").header("Authorization", "Bearer "))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void unauthorizedBadToken() throws Exception {
        mockMvc.perform(get("/").header("Authorization", "Bearer badtoken"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void okValidToken() throws Exception {
        mockMvc.perform(get("/").header("Authorization", "Bearer " + getValidToken("testUser", 1L)))
                .andExpect(status().isOk());
    }

    private String getValidToken(String userName, Long userId) {
        return Jwts.builder()
                .claim("userId", userId)
                .claim("userName", userName)
                .signWith(Keys.hmacShaKeyFor(jwtSignatureKey.getBytes()))
                .compact();
    }
}
