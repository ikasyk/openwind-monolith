package org.openwind.core.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openwind.core.security.SecurityAuthorizationFilter;
import org.openwind.core.security.SecurityConstants;
import org.openwind.core.security.SecurityHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@EnableWebSecurity
@Slf4j
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {
    @PostConstruct
    public void init() {
        if (StringUtils.isBlank(System.getenv(SecurityConstants.SECURITY_TOKEN_SECRET_KEY_PROPERTY))) {
            log.warn("WARNING: Highly recommend override security signature key using environment variable!");
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SecurityAuthorizationFilter filter = new SecurityAuthorizationFilter(authenticationManager(),
                SecurityHelper.getSignatureKey(getApplicationContext()),
                getApplicationContext());

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/users", "/api/users/authorize").permitAll()
                .antMatchers("/api/**").fullyAuthenticated()
                .and()
                .addFilter(filter)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
