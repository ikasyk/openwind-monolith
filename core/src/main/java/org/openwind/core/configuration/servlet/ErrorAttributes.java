package org.openwind.core.configuration.servlet;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ErrorAttributes extends DefaultErrorAttributes {

    private static final String RESPONSE_ERROR_ATTR_NAME = "error";
    private static final String RESPONSE_ERRORS_ATTR_NAME = "errors";

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);

        // Remove redundant error message text
        errorAttributes.remove(RESPONSE_ERROR_ATTR_NAME);

        simplifyFieldsErrorMessages(errorAttributes);

        return errorAttributes;
    }

    /**
     * <p>Simplifies error fields errors messages with the goal of readability and encapsulation.</p>
     *
     * <p>Applies template {@code errors} attribute like in example below:</p>
     * <pre>
     * "errors": [
     *      {"login": "Field cannot be empty"}
     * ]
     * </pre>
     *
     * @param errorAttributes
     */
    private void simplifyFieldsErrorMessages(Map<String, Object> errorAttributes) {
        Object errorsAttr = errorAttributes.get(RESPONSE_ERRORS_ATTR_NAME);
        if (!(errorsAttr instanceof List)) {
            return;
        }

        @SuppressWarnings("unchecked")
        List<FieldError> errors = (List<FieldError>) errorsAttr;

        Map<String, String> newErrors = new HashMap<>();
        for (FieldError error : errors) {
            newErrors.put(error.getField(), error.getDefaultMessage());
        }
        errorAttributes.put(RESPONSE_ERRORS_ATTR_NAME, newErrors);
    }
}
