package org.openwind.core.util;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.InvocationTargetException;

public class ReflectionUtils {
    public static <T> Object createProbe(Class<?> clazz, String fieldName, T fieldValue) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Object probe = ConstructorUtils.invokeConstructor(clazz);
        FieldUtils.writeField(probe, fieldName, fieldValue, true);
        return probe;
    }
}
