package org.openwind.core.security.model;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserAuthorizationToken extends UsernamePasswordAuthenticationToken {
    private static final long serialVersionUID = 1L;

    private String userName;

    public UserAuthorizationToken(Object principal, Object credentials, String userName) {
        super(principal, credentials);
        this.userName = userName;
    }

    public UserAuthorizationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String userName) {
        super(principal, credentials, authorities);
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
