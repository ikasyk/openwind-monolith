package org.openwind.core.security;

import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.openwind.core.security.dto.UserAuthority;
import org.openwind.core.security.model.UserAuthorizationToken;
import org.springframework.context.ApplicationContext;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class SecurityHelper {
    public static final String USER_ID_CLAIM = "userId";
    public static final String USER_NAME_CLAIM = "userName";

    public static String getTokenFromRequest(HttpServletRequest request) {
        String header = request.getHeader(SecurityConstants.AUTH_HEADER);

        if (StringUtils.isNotBlank(header) && header.startsWith(SecurityConstants.AUTH_TOKEN_PREFIX)) {
            return header.substring(SecurityConstants.AUTH_TOKEN_PREFIX.length());
        }

        return null;
    }

    public static JwtParser getTokenParser(SecretKey securityKey) {
        return Jwts.parserBuilder()
                .setSigningKey(securityKey)
                .build();
    }

    public static <T> T getClaimFromToken(String claim, String token, JwtParser parser, Class<T> type) {
        Jws<Claims> jws = parser.parseClaimsJws(token);
        return jws.getBody().get(claim, type);
    }

    public static String getUserNameFromToken(String token, JwtParser parser) {
        return getClaimFromToken(USER_NAME_CLAIM, token, parser, String.class);
    }

    public static Long getUserIdFromToken(String token, JwtParser parser) {
        return getClaimFromToken(USER_ID_CLAIM, token, parser, Long.class);
    }

    public static UserAuthorizationToken getAuthorizationTokenOrNull(Long userId, String userName, List<UserAuthority> authorities) {
        if (StringUtils.isNotBlank(userName) && isNotZero(userId)) {
            return new UserAuthorizationToken(userId, null, authorities, userName);
        }
        return null;
    }

    public static SecretKey getSignatureKey(ApplicationContext applicationContext) {
        String token = applicationContext.getEnvironment().getProperty(SecurityConstants.SECURITY_TOKEN_SECRET_KEY_PROPERTY);

        if (token == null) {
            throw new IllegalArgumentException(
                    String.format("Property %s is required for security initialization!", SecurityConstants.SECURITY_TOKEN_SECRET_KEY_PROPERTY)
            );
        }

        return SecurityConstants.JWT_TOKEN_ALGORITHM(token);
    }

    private static boolean isNotZero(Long number) {
        return number != null && !number.equals(NumberUtils.LONG_ZERO);
    }
}
