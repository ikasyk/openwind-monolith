package org.openwind.core.security;

import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public class SecurityConstants {
    /**
     * Prefix that will be specified before token in header value
     */
    public static final String AUTH_TOKEN_PREFIX = "Bearer ";

    /**
     * Name of header which must be provided in every request for authorization
     */
    public static final String AUTH_HEADER = "Authorization";

    /**
     * Time (in seconds) in which token will be expired. Expiration date counts as:
     * {@code new Date() + AUTH_TOKEN_LIFETIME}
     */
    public static final int AUTH_TOKEN_LIFETIME = 3600;

    /**
     * Algorithm that will be used for token encode
     */
    public static SecretKey JWT_TOKEN_ALGORITHM(String secret) {
        return Keys.hmacShaKeyFor(secret.getBytes());
    }

    /**
     * Property with token signature key
     */
    public static String SECURITY_TOKEN_SECRET_KEY_PROPERTY = "security.jwt.token-secret";
}
