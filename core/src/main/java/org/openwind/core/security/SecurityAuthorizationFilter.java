package org.openwind.core.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.ThreadContext;
import org.openwind.core.security.dao.UserDetailsDao;
import org.openwind.core.security.dto.UserAuthority;
import org.openwind.core.security.model.UserAuthorizationToken;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
public class SecurityAuthorizationFilter extends BasicAuthenticationFilter {
    private ApplicationContext applicationContext;

    private JwtParser tokenParser;

    public SecurityAuthorizationFilter(AuthenticationManager authenticationManager, SecretKey jwtSignatureKey,
                                       ApplicationContext applicationContext) {
        super(authenticationManager);
        this.tokenParser = SecurityHelper.getTokenParser(jwtSignatureKey);
        this.applicationContext = applicationContext;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            String token = SecurityHelper.getTokenFromRequest(request);

            if (token != null) {
                UserAuthorizationToken authorizationToken = getAuthentication(token);

                ThreadContext.put("sessionId", request.getHeader("SessionId")); //may be null
                ThreadContext.put("ipAddress", request.getRemoteAddr());
                ThreadContext.put("userName", authorizationToken.getUserName());

                SecurityContextHolder.getContext().setAuthentication(authorizationToken);
            }

        } catch (ExpiredJwtException e) {
            log.warn("JWT Token expired. {}", e.getMessage());
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        } catch (Exception e) {
            log.error("Exception during JWT authorization", e);
        }

        chain.doFilter(request, response);

        ThreadContext.clearAll();
    }

    private UserAuthorizationToken getAuthentication(String token) {
        String userName = SecurityHelper.getUserNameFromToken(token, tokenParser);
        Long userId = SecurityHelper.getUserIdFromToken(token, tokenParser);

        List<UserAuthority> userAuthorities = UserDetailsDao.getUserAuthorities(userId, applicationContext);

        return SecurityHelper.getAuthorizationTokenOrNull(userId, userName, userAuthorities);
    }
}
