package org.openwind.core.security.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.openwind.core.security.dto.UserAuthority;
import org.springframework.context.ApplicationContext;

import javax.sql.DataSource;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.List;

@Slf4j
public class UserDetailsDao {
    private static final String SELECT_USER_AUTHORITIES = "SELECT ud.UD_VALUE FROM USER_DATA ud WHERE ud.UD_KEY = ? AND ud.UD_USER = ?";

    private static final String UD_AUTHORITIES_KEY = "USER_AUTHORITIES";

    public static List<UserAuthority> getUserAuthorities(Long userId, ApplicationContext applicationContext) {
        DataSource ds = (DataSource) applicationContext.getBean("dataSource");

        try (Connection connection = ds.getConnection()) {

            try (PreparedStatement statement = connection.prepareStatement(SELECT_USER_AUTHORITIES)) {
                statement.setString(1, UD_AUTHORITIES_KEY);
                statement.setLong(2, userId);

                try (ResultSet resultSet = statement.getResultSet()) {
                    if (resultSet.next()) {
                        Clob clob = resultSet.getClob("UD_VALUE");

                        ObjectMapper mapper = new ObjectMapper();

                        return mapper.readValue(clob.getCharacterStream(), List.class);
                    }
                }
            }

        } catch (Exception ex) {
            log.error("Unable to retrieve user authorities due to exception", ex);
        }

        return Collections.emptyList();
    }
}
